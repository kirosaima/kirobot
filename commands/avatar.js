const Discord = require('discord.js');
exports.run = async (client, config, message, args) =>
    {
        var member = message.mentions.members.first();
        var roles = [];
        var rolesCount = 0;
        if(member) {
            const embed = new Discord.RichEmbed()
                  .setTitle(`@${member.user.username}`)
                  .setColor(0xec33d0)
                  .setAuthor(client.user.username)
                  .setImage(member.user.avatarURL)

            message.channel.send({embed});            
        } else {
            const embed = new Discord.RichEmbed()
                    .setTitle('@'+ message.author.username)
                    .setAuthor(client.user.username)
                    .setColor(0xec33d0)
                    .setImage(message.author.avatarURL);
            message.channel.send({embed});
        }
    }
