exports.run = (client, config, message, args) =>
    {
        if(!message.member.hasPermission("BAN_USER"))
            return message.reply("You require greater permissions to access this command!");

        let member = message.mentions.members.first();
        if(!member)
            return message.reply("Please mention a user.");
        if(!member.bannable)
            return message.reply("I cannot boot this user.")
        let reason = args.slice(1).join(' ');
        if(!reason)
            return message.reply("Please give a reason for the banning.");

        member.ban(reason)
            .catch(error => message.reply
                   (`Sorry ${message.author} I couldn't ban because of : ${error}`));
        message.reply(`${member.user.tag} has been banned by ${message.author.tag} because : ${reason}`);
    }
