exports.run = (client, config, message, args) =>
    {
        let count = 2;
        if(!message.member.hasPermission("MANAGE_MESSAGES")) {
            return message.reply("You require greater permissions to access this command!");
        } else {
            if(args.length > 0) 
                count = parseInt(args);

            message.channel.send('Purging ' + count + ' Messages');
            message.channel.bulkDelete(count + 1);
        }
    }
