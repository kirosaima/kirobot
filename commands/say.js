exports.run = (client, config, message, args) =>
{
    let str = '';

    args.forEach(function(element) {
        str += element.toString() + ' ';
    });

    message.delete().catch(console.error);
    message.channel.send(`***${str}***`);
}
