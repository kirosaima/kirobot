exports.run =(client, config, message, args) =>
    {
        var cmdDir = './commands/';
        var fs = require('fs');

        fs.readdir(cmdDir, (err, files) =>
                   {
                       var cmdList = '';

                       files.forEach(file => {
                           if(file.toLowerCase().endsWith(".js")) {
                               cmdList += file.toLowerCase().split(".js")[0] + '\n';
                           }
                       });

                       message.channel.send('```' + cmdList.toString() + '```');
                   })
    }
