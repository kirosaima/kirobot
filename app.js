const Discord = require('discord.js');

const client = new Discord.Client();

const config = require("./config/config.json");

client.on('ready', () =>
{
    console.log(`Bot has started, with ${client.users.size} users, in ${client.channels.size} channels.`);
    client.user.setGame('Prefix = k!');
});

client.on("guildCreate", guild => {
    console.log(`New guild joined: ${guild.name} (id: ${guild.id}). This guild has ${guild.memberCount} members!`);
    client.user.setGame(`on ${client.gui.size} servers`);
});

client.on("guildDelete", guild => {
    console.log(`I have been removed from: ${guild.name} (id: ${guild.id})`);
    client.user.setGame(`on ${client.guilds.size - 1} servers`);
});

client.on('message', async message =>
{
    if(message.author.bot) return;
    if(message.content.indexOf(config.prefix) !== 0) return;
    console.log(`${message.author.tag} [ ${message.guild}#${message.channel.name} ]  :  ${message.content}`);
    message.channel.startTyping();

    const args = message.content.slice(config.prefix.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();

    try {
        let commandFile = require(`./commands/${command}.js`);
        commandFile.run(client, config, message, args);
    } catch (err) {
        console.error(err);
        message.channel.sendMessage(err.message);
    }

    message.channel.stopTyping();
});

client.login(config.token);
